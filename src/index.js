import React from 'react';
import ReactDOM from 'react-dom';

import {Provider} from 'react-redux'

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import App from './components/App'
import reducer from './components/Reduser'
import decrement from './components/sagas'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(decrement)

function render(){
	ReactDOM.render(
	<Provider store ={store}>
		<App />
	</Provider>,
document.getElementById('root')
	); 
}

render()
store.subscribe(render)
