import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {new_counter} from './actions'

class Butt extends Component {
	render(){
  		return (
      		<div > 
      			<button onClick = {() => this.props.counter('GENERATE_NUM') }>choose number</button>	
      		</div>
    	)
	}
}


function mapDispatchToProps(dispatch){
	return bindActionCreators({counter: new_counter},dispatch)
}

export default connect(null, mapDispatchToProps )(Butt);
