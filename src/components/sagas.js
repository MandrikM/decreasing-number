import { put, takeLatest } from 'redux-saga/effects'

const delay = (ms) => new Promise(res => setTimeout(res, ms))

export default function* decrement() {
  yield takeLatest("GENERATE_NUM", decrementAsync)
}

export function* decrementAsync(action) {
	let num = Math.floor(Math.random() * 9)+2
	let start_dalay = 1000
	yield put({ type: 'NEW_NUM' , payload: num})
  	let half_num = num / 2
  	let i = 1
  	while (i < half_num){
  		yield delay(start_dalay)
  		num = num - 1
  		yield put({ type: 'NEW_NUM' , payload: num})
  		start_dalay =start_dalay- 100
  		i++
  	}
  	
  	while (i<= 2*half_num ){
  
  		yield delay(start_dalay)
  		num = num - 1
  		yield put({ type: 'NEW_NUM' , payload: num})
  		start_dalay = start_dalay + 100
  		i++
  	}

}
