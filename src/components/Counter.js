import React, {Component} from 'react'
import {connect} from 'react-redux'

class Counter extends Component {
	render (){
  return (
      	<div >	
      		<hr/>
      	{this.props.current_num}
      	</div>
    )
}

}
function mapStateToProps(state){
	return{
		current_num: state
	}
}

export default connect (mapStateToProps)(Counter);