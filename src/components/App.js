import React from 'react';
import Butt from './Butt'
import Counter from './Counter'

function App() {
  return (
      	<div >
      		<Butt/>
      		<Counter/>			
      	</div>
    );
}

export default App;
